package cz.vse.java.xvalm00.adventuracv.util;

public interface SubjectOfChange {

    void registerObserver(Observer observer);
    void unregisterObserver(Observer observer);
    void notifyObservers();

}
