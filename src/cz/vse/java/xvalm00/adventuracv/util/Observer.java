package cz.vse.java.xvalm00.adventuracv.util;

public interface Observer {

    void update();

}
