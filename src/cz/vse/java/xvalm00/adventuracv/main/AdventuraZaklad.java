package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class AdventuraZaklad extends Application {

    private TextArea konzole;
    private HerniPlocha herniPlocha;
    private IHra hra;
    private MenuBar menuBar = new MenuBar();
    private BorderPane borderPane = new BorderPane();
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;
    private TextField uzivatelskyVstup = new TextField();

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = Hra.getSingleton();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else {
                System.out.println("Neplatny parametr.");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) {

        VBox hlavniVBox = new VBox();
        hlavniVBox.getChildren().addAll(menuBar, borderPane);

        pripravMenu();


        FlowPane spodniPanel = new FlowPane();

        konzole = new TextArea();
        hra = Hra.getSingleton();
        konzole.setText(hra.vratUvitani());
        borderPane.setCenter(konzole);
        konzole.setEditable(false);

        pripravTextInput(uzivatelskyVstup);

        Label zadejPrikazLabel = new Label("Zadej příkaz: ");
        zadejPrikazLabel.setFont(Font.font("Ariel", FontWeight.BOLD, 16));
        spodniPanel.getChildren().addAll(zadejPrikazLabel, uzivatelskyVstup);
        spodniPanel.setAlignment(Pos.CENTER);

        borderPane.setBottom(spodniPanel);

        panelBatohu = new PanelBatohu(hra.getBatoh());
        borderPane.setLeft(panelBatohu.getPanel());

        HerniPlan plan = hra.getHerniPlan();
        herniPlocha = new HerniPlocha(plan);
        borderPane.setTop(herniPlocha.getAnchorPane());

        panelVychodu = new PanelVychodu(plan);
        borderPane.setRight(panelVychodu.getListView());

        Scene scene = new Scene(hlavniVBox, 600, 450);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Adventura");
        uzivatelskyVstup.requestFocus();
        primaryStage.show();


        primaryStage.setOnCloseRequest(event -> ukonceniJavaFxAplikace());
    }

    private void pripravMenu() {

        Menu souborMenu = new Menu("Soubor");
        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));
        MenuItem novaHra = new MenuItem("Nova hra", novaHraIkonka);
        novaHra.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        novaHra.setOnAction(event -> {
            hra = Hra.restartHry();
            konzole.setText(hra.vratUvitani());
            herniPlocha.restartHry(hra.getHerniPlan());
            panelVychodu.restartHry(hra.getHerniPlan());
            panelBatohu.restartHry(hra.getBatoh());
            uzivatelskyVstup.requestFocus();
        });

        MenuItem konec = new MenuItem("Konec");
        konec.setOnAction(event -> System.exit(0));

        SeparatorMenuItem separator = new SeparatorMenuItem();
        souborMenu.getItems().addAll(novaHra, separator, konec);

        Menu napovedaMenu = new Menu("Nápověda");
        MenuItem oAplikaci = new MenuItem("O aplikaci");
        MenuItem napovedaHtm = new MenuItem("Nápověda k aplikaci");
        napovedaMenu.getItems().addAll(oAplikaci, napovedaHtm);

        oAplikaci.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Graficka adventura");
            alert.setHeaderText("JavaFX adventura");
            alert.setContentText("verze LS 2020");
            alert.showAndWait();
        });

        napovedaHtm.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource("/zdroje/napoveda.htm").toExternalForm());
            stage.setScene(new Scene(webView, 500, 500));
            stage.show();
        });


        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
    }

    private void pripravTextInput(TextField uzivatelskyVstup) {
        synchronized (this) {
            uzivatelskyVstup.setOnAction(event -> {
                String prikaz = uzivatelskyVstup.getText();
                konzole.appendText("\n" + prikaz + "\n");
                uzivatelskyVstup.setText("");
                String odpovedHry = hra.zpracujPrikaz(prikaz);
                konzole.appendText("\n" + odpovedHry + "\n");
                if (hra.konecHry()) {
                    uzivatelskyVstup.setEditable(false);
                    konzole.appendText("\nHra se ukončí za tři vteřiny.");

                    Task<Void> task = new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            Thread.sleep(3000);
                            return null;
                        }
                    };

                    task.setOnSucceeded(event2 -> ukonceniJavaFxAplikace());

                    new Thread(task).start();
                }
            });
        }
    }

    private void ukonceniJavaFxAplikace() {
        Platform.exit();
        System.exit(0);
    }

}
