package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.Batoh;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import java.io.InputStream;
import java.util.Set;

public class PanelBatohu implements Observer {

    private final VBox vbox = new VBox();
    private final FlowPane panelVeci = new FlowPane();
    private Batoh batoh;

    public PanelBatohu(Batoh batoh) {
        this.batoh = batoh;
        init();
        batoh.registerObserver(this);
    }

    private void init() {
        vbox.setPrefWidth(100);
        Label label = new Label("Seznam věcí:");
        vbox.getChildren().addAll(label, panelVeci);

        nactiObrazkyVeci();
    }


    private void nactiObrazkyVeci() {
        panelVeci.getChildren().clear();

        Set<String> mnozinaVeci = batoh.seznamPredmetu();
        for (String vec : mnozinaVeci) {
            String nazevObrazku = "/zdroje/" + vec + ".jpg";
            InputStream inputStream = PanelBatohu.class.getResourceAsStream(nazevObrazku);
            Image image = new Image(inputStream, 100, 100, false, false);
            ImageView imageView = new ImageView(image);
            panelVeci.getChildren().add(imageView);
        }
    }

    @Override
    public void update() {
        nactiObrazkyVeci();
    }

    public Node getPanel() {
        return vbox;
    }

    public void restartHry(Batoh batoh) {
        this.batoh = batoh;
        this.batoh.registerObserver(this);
        update();

    }
}
