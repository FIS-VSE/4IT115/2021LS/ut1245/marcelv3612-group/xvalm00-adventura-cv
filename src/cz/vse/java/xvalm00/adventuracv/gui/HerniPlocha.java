package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;


public class HerniPlocha implements Observer {

    private final AnchorPane anchorPane = new AnchorPane();
    private Circle aktualniPozice;
    private HerniPlan herniPlan;

    public HerniPlocha(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;

        init();

        herniPlan.registerObserver(this);
    }

    private void init() {
        ImageView herniPlanImageView = new ImageView(new Image(HerniPlocha.class.getResourceAsStream
                ("/zdroje/herniPlan.png"), 400, 250, false, false));

        aktualniPozice = new Circle(10, Paint.valueOf("red"));

        nastavPoziciHrace();

        anchorPane.getChildren().addAll(herniPlanImageView, aktualniPozice);
        herniPlan.registerObserver(this);
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    @Override
    public void update() {
        nastavPoziciHrace();
    }

    private void nastavPoziciHrace() {
        AnchorPane.setTopAnchor(aktualniPozice, herniPlan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(aktualniPozice, herniPlan.getAktualniProstor().getPosLeft());
    }

    public void restartHry(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
        this.herniPlan.registerObserver(this);
        update();
    }
}
